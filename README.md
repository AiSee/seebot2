# SeeBot v2.4.4

Starcraft 2 protoss bot coded in Python 3.6. Current version turtles
into skytoss and able to defeat strongest embedded opponent CheatInsane.
Also there is macro-oriented version here:
https://bitbucket.org/AiSee/seebot2/src/v2.3/

## Requirements
* [Python 3.6+](https://www.python.org/downloads/)
* [Python-sc2](https://github.com/Dentosal/python-sc2)
(```pip install sc2```)

## How to run
1. Install the requirements and download the repository.
2. Type ```python seebot.py``` in the console.

## Purpose
This code is published for educational purposes only. If you are
planning to use it for building your own bot, you should modify it to a
state when your bot's behaviour will be totally different